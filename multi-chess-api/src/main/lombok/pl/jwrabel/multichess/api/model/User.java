package pl.jwrabel.multichess.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public @Data class User {
    private String username;
    private String password;
}
