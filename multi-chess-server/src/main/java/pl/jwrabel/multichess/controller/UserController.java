package pl.jwrabel.multichess.controller;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriComponentsBuilder;
import pl.jwrabel.multichess.api.model.User;
import pl.jwrabel.multichess.model.UserDAO;
import pl.jwrabel.multichess.model.UserMapper;
import pl.jwrabel.multichess.repository.UserRepository;

import javax.sql.DataSource;

@Slf4j
@org.springframework.web.bind.annotation.RestController
public class UserController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    UserRepository userRepository;

    @Autowired
    DataSource dataSource;

    @RequestMapping(value = "/userAuthenticated/", method = RequestMethod.GET)
    public ResponseEntity<Boolean> authenticateUser(@RequestParam(name = "username") String username, @RequestParam(name = "password") String password) {
        logger.info("Authenticating user {}", username);

        if (username == null || password == null) {
            return new ResponseEntity<>(Boolean.FALSE, HttpStatus.GONE);
        }

        UserDAO userDAO = UserMapper.toUserDAO(username, password);
        if (userDAO == null) {
            logger.error("Could not create user object.");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        UserDAO user = userRepository.findByNameAndPasswordMD5(userDAO.getName(), userDAO.getPasswordMD5());
        return new ResponseEntity<>(Boolean.valueOf(user != null), HttpStatus.FOUND);
    }

    @RequestMapping(value = "/user/", method = RequestMethod.POST)
    public ResponseEntity<Void> createUser(@RequestBody User user, UriComponentsBuilder ucBuilder, @AuthenticationPrincipal final UserDetails authUser) {
        logger.info("Creating user [name={}]", user.getUsername());

        UserDAO userDAO = UserMapper.toUserDAO(user);
        if (userDAO == null) {
            logger.error("Could not create user object.");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (userRepository.findByName(userDAO.getName()) != null) {
            logger.warn("A UserDAO with name {}  already exist", userDAO.getName());
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        userRepository.save(userDAO);
        logger.info("New user user [name={}] has been saved", user.getUsername());

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(userDAO.getName()).toUri());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }
}
