package pl.jwrabel.multichess;

import de.codecentric.boot.admin.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan(basePackages = {
        "pl.jwrabel.multichess.model"
})
@EnableJpaRepositories(basePackages = {
        "pl.jwrabel.multichess.repository"
})
@Configuration
@EnableAutoConfiguration
@EnableAdminServer
public class MultiChessServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MultiChessServerApplication.class, args);
    }
}
