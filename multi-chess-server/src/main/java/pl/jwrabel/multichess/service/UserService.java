package pl.jwrabel.multichess.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.jwrabel.multichess.model.UserDAO;
import pl.jwrabel.multichess.repository.UserRepository;

import java.util.Collections;

@Service
@Transactional
public class UserService
        implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        UserDAO userDao = userRepository.findByName(name);
        return new User(userDao.getName(), userDao.getPasswordMD5(), true, true, true, true, Collections.emptyList());
    }
}
