package pl.jwrabel.multichess.repository;

import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import pl.jwrabel.multichess.model.UserDAO;

public interface UserRepository extends Repository<UserDAO, Long>  {
    void save(UserDAO user);

    UserDAO findByName(@Param("name") String name);

    UserDAO findByNameAndPasswordMD5(@Param("name") String name, @Param("passwordMD5") String passwordMD5);
}
