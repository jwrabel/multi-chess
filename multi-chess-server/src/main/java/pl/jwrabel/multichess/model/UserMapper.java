package pl.jwrabel.multichess.model;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import pl.jwrabel.multichess.api.model.User;

public class UserMapper {
    private static BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    public static UserDAO toUserDAO(User user) {
        if (user == null || user.getPassword() == null) {
            return null;
        }

        String encryptedPassword = passwordEncoder.encode(user.getPassword());
        return UserDAO.builder().name(user.getUsername()).passwordMD5(encryptedPassword).build();
    }

    public static UserDAO toUserDAO(String username, String password) {
        User user = User.builder().username(username).password(password).build();
        return toUserDAO(user);
    }
}
