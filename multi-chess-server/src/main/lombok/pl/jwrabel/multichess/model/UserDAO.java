package pl.jwrabel.multichess.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users")
public
@Data
class UserDAO {
    @Id
    private String name;
    private String passwordMD5;
}
